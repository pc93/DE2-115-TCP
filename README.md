# DE2-115-TCP

Projekt zawiera zmodyfikowane pliki szablonu Simple Socket Server RGMII pozwalające na poprawne uruchomienie oprogramowania procesora Nios II oraz komunikacje z układem DE2-115 poprzez protokół TCP.

Aby projekt zadziałał skopiuj (oraz nadpisz) wszystkie pliki i katalogi dostępne w tym repozytorium do katalogu własnego projektu wygenerowanego z szablonu (np: ..\DE2_115_WEB_SERVER_RGMII\Software\MySimpleSocketServer\)
